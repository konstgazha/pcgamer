from django import forms
import datetime


class AnalysisForm(forms.Form):
    word = forms.CharField(label='Enter your request:', max_length=100)
    min_date = forms.CharField(label='from', initial='01.01.2016')
    max_date = forms.CharField(label='to', initial=datetime.datetime.now().date().strftime('%d.%m.%Y'))


class ReviewForm(forms.Form):
    date = forms.CharField(label='Enter the date', initial='30.12.2016')
