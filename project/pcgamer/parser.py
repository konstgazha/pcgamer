from bs4 import BeautifulSoup
from dateutil.parser import parse
from datetime import datetime
from enum import Enum
from fake_useragent import UserAgent
from os import listdir
from os.path import isfile, join
from peewee import *
from urllib.request import Request, urlopen
from time import sleep
from queue import Queue
import ast
import threading
import time
import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
import re
import pymorphy2
import random


__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

morph = pymorphy2.MorphAnalyzer()
mysql_db = MySQLDatabase('pcgamer',
                         host='localhost',
                         user='root',
                         password='')


class BaseModel(Model):
    class Meta:
        database = mysql_db


class OriginalReview(BaseModel):
    title = CharField()
    link = CharField()
    date = DateField()
    content = CharField()


class MorphReview(BaseModel):
    content = CharField()
    review = ForeignKeyField(OriginalReview)


class Task(Enum):
    NEWS_LINK = 1
    NEWS_CONTENT = 2


class NewsType(Enum):
    ORIGINAL = 1
    MORPH = 2


class Parser:
    @staticmethod
    def get_soup(url):
        sleep((random.randint(7, 10)) * 0.1)
        request = Request(url, headers={'User-Agent': UserAgent().random})
        response = urlopen(request)
        return BeautifulSoup(response.read().decode('utf-8'), 'html5lib')


class PCGamer(Parser):
    def __init__(self):
        self.news_links = []
        self.last_page = 10
        self.first_page = 1
        self._threads_quantity = 4

    domain = 'http://www.pcgamer.com'
    empty_articles = []

    def set_threads_quantity(self, value):
        if value < 1:
            raise ValueError("At least one thread required.")
        self._threads_quantity = value

    def set_last_page(self, value):
        if value < 1:
            raise ValueError("At least one page required.")
        if self.last_page < self.first_page:
            raise ValueError("Last page should be bigger then first page.")
        self.last_page = value

    def set_last_page(self, value):
        if value < 1:
            raise ValueError("At least one page required.")
        if self.last_page < self.first_page:
            raise ValueError("First page should be fewer then last page.")
        self.first_page = value

    @staticmethod
    def get_empty_articles():
        reviews = OriginalReview.select().where(OriginalReview.content == '')
        ids = [reviews[i].id for i in range(len(reviews))]
        return ids

    @staticmethod
    def get_news_page(page):
        url = 'http://www.pcgamer.com/reviews/page/%s' % str(page)
        return Parser.get_soup(url)

    def find_last_page(self, soup):
        regex = '=(.+)'
        self.last_page = soup.find_all('a', class_='navigation')[1].get('href')
        self.last_page = re.search(regex, self.last_page).group(1)

    def threaded_parse(self, task):
        queue = Queue()
        for x in range(self._threads_quantity):
            thread = ThreadedParser(queue, task)
            thread.daemon = True
            thread.start()

        if task == Task.NEWS_CONTENT:
            ids = self.get_empty_articles()
            for i in range(len(ids)):
                queue.put(ids[i])
        elif task == Task.NEWS_LINK:
            for i in range(self.first_page, self.last_page):
                queue.put(i)
        queue.join()

    @staticmethod
    def parse_news_content(link):
        soup = Parser.get_soup(link)
        content = soup.find('div', {'id': 'article-body'}).getText().strip()
        return content

    @staticmethod
    def morph_content(content):
        content = list(map(lambda x: x.strip(), re.sub('\W', ' ', content).split(' ')))
        content = [x for x in content if not x == '']
        morph_content = list(map(lambda x: morph.parse(x)[0].normal_form, content))
        return morph_content

    @staticmethod
    def load_original_articles(date):
        articles = OriginalReview.select().group_by(OriginalReview.date).where(OriginalReview.date ==
                                                                               parse(date, dayfirst=True))
        return articles


class ThreadedParser(threading.Thread):
    def __init__(self, queue, task):
        self.queue = queue
        self.task = task
        threading.Thread.__init__(self)

    def run(self):
        if self.task == Task.NEWS_LINK:
            while True:
                page = self.queue.get()

                soup = PCGamer.get_news_page(page)
                print(page)
                #print(soup.find('div', class_='content'))
                for review in soup.find_all('div', {'data-page': 1}):

                    title = re.sub(' review', '', review.find('h3', class_='article-name').getText(), 1)
                    date = parse(review.find('time', class_='published-date relative-date').get('datetime')[:10])
                    #review.find('time', class_='published-date relative-date').getText()
                    #print(title)
                    link = review.a.get('href')
                    OriginalReview().create(title=title,
                                            date=date,
                                            link=link)
                self.queue.task_done()

        elif self.task == Task.NEWS_CONTENT:
            while True:
                review_id = self.queue.get()
                review = OriginalReview.get(OriginalReview.id == review_id)
                print(review.title)
                content = PCGamer.parse_news_content(review.link)
                morph_content = PCGamer.morph_content(content)
                OriginalReview.update(content=content).where(OriginalReview.id == review_id).execute()
                MorphReview.create(content=morph_content, review=review_id)
                self.queue.task_done()


class TermFrequency:
    def __init__(self, term, time_range=''):
        self.term = term
        self.time_range = time_range
        self.result = []

    def frequency(self, words):
        count = 0
        for word in words:
            if self.term == word:
                count += 1
        return count

    def term_frequency(self, words):
        return self.frequency(words) / len(words)

    def compute(self, min_time='', max_time='', tf=False):
        if not self.time_range:
            self.time_range = OriginalReview.select(fn.Min(OriginalReview.date),
                                                    fn.Max(OriginalReview.date)).scalar(as_tuple=True)

        if not max_time or parse(str(max(self.time_range))) < parse(max_time, dayfirst=True):
            max_time = max(self.time_range)
        else:
            max_time = parse(max_time, dayfirst=True)
        if not min_time or parse(str(min(self.time_range))) > parse(min_time, dayfirst=True):
            min_time = min(self.time_range)
        else:
            min_time = parse(min_time, dayfirst=True)

        reviews = OriginalReview.select().group_by(OriginalReview.date).where(min_time <= OriginalReview.date,
                                                                              OriginalReview.date <= max_time)

        if tf:
            for i in reviews:
                self.result.append([i.date, self.term_frequency(ast.literal_eval(MorphReview.get(MorphReview.review ==
                                                                                                 i.id).content))])
        else:
            for i in reviews:
                self.result.append([i.date, self.frequency(ast.literal_eval(MorphReview.get(MorphReview.review ==
                                                                                            i.id).content))])
        return np.array(self.result)


prs = PCGamer()
prs.threaded_parse(task=Task.NEWS_LINK)
prs.threaded_parse(task=Task.NEWS_CONTENT)

#tf = TermFrequency('good')
#print(tf.compute(min_time='01.01.2016', max_time='01.12.2016', tf=True))
