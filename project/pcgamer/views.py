from django.shortcuts import render
from graphos.sources.simple import SimpleDataSource
from graphos.renderers.gchart import LineChart
from .forms import AnalysisForm, ReviewForm
from .parser import TermFrequency, PCGamer
import numpy as np


def index(request):
    context = {}

    if request.method == 'POST':
        analysis_form = AnalysisForm(request.POST)
        if analysis_form.is_valid():
            word = analysis_form.cleaned_data['word']
            min_date = str(analysis_form.cleaned_data['min_date'])
            max_date = str(analysis_form.cleaned_data['max_date'])

            tf = TermFrequency(word)
            data = tf.compute(min_time=min_date, max_time=max_date).tolist()

            #data = np.hstack(data).tolist()
            for i, row in enumerate(data):
                for j, elem in enumerate(row[1:]):
                    data[i][j+1] = int(elem)
            data = [['Data', word]] + data

            data_source = SimpleDataSource(data=data)
            chart = LineChart(data_source)

            context.update({'chart': chart, 'form': analysis_form})
    else:
        analysis_form = AnalysisForm()
        context.update({'form': analysis_form})
    return render(request, 'index.html', context)


def review(request):
    context = {}
    if request.method == 'POST':
        review_form = ReviewForm(request.POST)
        if review_form.is_valid():
            reviews = PCGamer.load_original_articles(review_form.cleaned_data['date'])
            links = []
            for rev in reviews:
                links.append(rev.link)
                print(rev.link)
            context.update({'form': review_form,
                            'reviews': reviews})
    else:
        review_form = ReviewForm()
        context.update({'form': review_form})
    return render(request, 'review.html', context)
